package spring.project.pet.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import spring.project.pet.repository.PostRepository;
import spring.project.pet.repository.UserRepository;
import spring.project.pet.model.Post;
import spring.project.pet.model.User;

@RestController
public class PostController {
	@Autowired
	private PostRepository postRepository;
	@Autowired
	private UserRepository userRepository;

	@GetMapping("/post")
	public String showPost(Model model) {
		model.addAttribute("post", new Post());
		return "post";
	}

	@PostMapping(value = "/process_post")
	public RedirectView processPost(Post post, HttpServletRequest request) {
		post.setContent(null);
		post.setUrl(null);
		User user = userRepository.findByEmail(request.getRemoteUser());
		post.setUser(user);
		postRepository.save(post);
		return new RedirectView(user.getEmail());
	}
}
