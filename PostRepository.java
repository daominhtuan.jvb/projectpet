package spring.project.pet.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import spring.project.pet.model.Post;

public interface PostRepository extends JpaRepository<Post, Long> {
	@Query("SELECT p FROM Post p WHERE id = ?1")
	public ArrayList<Post> findByUserId(Long id);
	
	@Query("SELECT p FROM Post p WHERE post_id = ?1")
	public Post findByPostId(Long postId);
}
